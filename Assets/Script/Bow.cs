﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bow : MonoBehaviour
{
    public Transform arrowSpawnPoint1;
    public Transform arrowSpawnPoint2;
  
    public GameObject arrowPrefab1;
    public GameObject arrowPrefab2;
   

    public float delayNormalAtk;
    public float delaySkillAtk;
  
    public static Bow instance;

    private void Awake()
    {
        instance = this;
    }

    public void Update()
    {
       
        if ( delayNormalAtk >0)
        {
            delayNormalAtk  -= Time.deltaTime;
        }
     if ( delaySkillAtk > 0)
        {
            delaySkillAtk -= Time.deltaTime;
        }
    
        if ( Input.GetButtonDown("Fire1"))
        {
            NormalAttack();
            
        }
        if (Input.GetKeyDown(KeyCode.Alpha3) && PlayerData.instance.currentLevel >= 2 && delaySkillAtk <= 0)
        {
            SkillAttack();
      
        }
    }

    public void NormalAttack()
    {
      

        if (delayNormalAtk >0)
        {
            Debug.Log("WAIT");
        }
        if (delayNormalAtk <= 0)
        {
           

          
            arrowPrefab1.SetActive(true);
            var arrow1 = Instantiate(arrowPrefab1, arrowSpawnPoint1.position, arrowSpawnPoint1.rotation);
            arrow1.GetComponent<Rigidbody>().velocity = arrowSpawnPoint1.forward * 20;
            arrowPrefab1.SetActive(false);
            delayNormalAtk = 2f;
            UIManager.instance.isAttackSoundOn = true;
            UIManager.instance.attackSoundSystem.SetActive(true);
            Invoke("EndSound", 0.5f);
        }

      


    }

    public void EndSound()
    {
        UIManager.instance.isAttackSoundOn = false;
        UIManager.instance.attackSoundSystem.SetActive(false);
    }
    public void SkillAttack()
    {
        if (PlayerData.instance.currentMP >=20 && delaySkillAtk <=0)
        {
            PlayerData.instance.TakeDamageMP(20);
            arrowPrefab1.SetActive(true);
            arrowPrefab2.SetActive(true);
            var arrow1 = Instantiate(arrowPrefab1, arrowSpawnPoint1.position, arrowSpawnPoint1.rotation);
            arrow1.GetComponent<Rigidbody>().velocity = arrowSpawnPoint1.forward * 50;
            var arrow2 = Instantiate(arrowPrefab2, arrowSpawnPoint2.position, arrowSpawnPoint2.rotation);
            arrow2.GetComponent<Rigidbody>().velocity = arrowSpawnPoint2.forward * 50;
            arrowPrefab1.SetActive(false);
            arrowPrefab2.SetActive(false);
            delaySkillAtk = 4f;
        }
       

        if ( PlayerData.instance.currentMP < 20 || delaySkillAtk >0)
        {
            Debug.Log("Not Enough MP");
        }
        
    }

       
    

      
        
    
}
