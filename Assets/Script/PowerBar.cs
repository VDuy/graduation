﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PowerBar : MonoBehaviour
{
    [Header ("PowerUI")]
    public Slider hpSlider;
    public Slider mpSlider;

    public static PowerBar instance;
    private void Awake()
    {
        instance = this;
    }
    
   
    public void Health(int hp)
    {
        hpSlider.value = PlayerData.instance.currentHP;
    }

    public void Mana(int mp)
    {
        mpSlider.value = PlayerData.instance.currentMP;
    }

    public void MaxHealth(int hp)
    {
        hp = PlayerData.instance.baseHP;
        hpSlider.maxValue = hp;
       
    }
    public void MaxMana(int mp)
    {
        mp = PlayerData.instance.baseMP;
        mpSlider.maxValue = mp;
        
    }
        
    
}
