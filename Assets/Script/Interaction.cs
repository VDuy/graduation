﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interaction : MonoBehaviour
{

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == ("Player"))
        {
          
            UIManager.instance.fBtn.SetActive(true);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == ("Player"))
        {
            UIManager.instance.isActiveFBtn = true;
           


            if (Input.GetKeyDown(KeyCode.F))
            {
                UIManager.instance.PressF();
                UIManager.instance.fBtn.SetActive(false);
                UIManager.instance.isShopOpen = true;
              
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == ("Player"))
        {
            UIManager.instance.isActiveFBtn = false;
            UIManager.instance.fBtn.SetActive(false);
            UIManager.instance.OnClickCloseShop();
        }

    }
}
