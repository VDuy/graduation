﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
//public class Data
//{
//    public int money;
//    public int health;
//    public int level;
//    public int attackDamage;
//    public int mana;
//    public int resistance;

//    public Data()
//    {
//        money = 10000;
//        health = PlayerData.instance.baseHP;
//        level = PlayerData.instance.currentLevel;
//        mana = PlayerData.instance.baseMP;
//        attackDamage = PlayerData.instance.totalAtk;
//        resistance = PlayerData.instance.totalDef;
//    }
//}


public class PlayerData : MonoBehaviour
{

    [Header("Data")]

    public int currentHP;
    public int currentMP;
    public int currentLevel;
    public int baseLevel = 1;
    public int baseATK = 30;
    public int baseDef = 25;
    public int totalAtk;
    public int totalDef;
    public int baseHP = 200;
    public int baseMP = 100;

    [Header("Bars")]
    public PowerBar hpBar;
    public PowerBar mpBar;
    [Header("Regen")]

    private WaitForSeconds hpRegen = new WaitForSeconds(5f);
    private WaitForSeconds mpRegen = new WaitForSeconds(5f);
    private Coroutine hpRegenCheck;
    private Coroutine mpRegenCheck;


    public float hpBtnDelayTime;
    public float mpBtnDelayTime;

    public static PlayerData instance;


    public bool levelUp = false;
    
    public GameObject lvlUpPanel;

    private PlayerData player;

    public PlayerData(PlayerData player)
    {
        this.player = player;
    }

    #region singleTon

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }

    }
    #endregion

    public void Start()
    {
        currentHP = baseHP;
        currentMP = baseMP;

    }

    // Update is called once per frame
    public void Update()
    {
        totalAtk = baseATK - MenuManager.instance.diff * 2;
        totalDef = baseDef - MenuManager.instance.diff * 2;

        if (currentHP > baseHP)
        {
            currentHP = baseHP;
        }
        if (currentMP > baseMP)
        {
            currentMP = baseMP;
        }
        
        if (hpBtnDelayTime >0f)
        {
            hpBtnDelayTime -= Time.deltaTime;
        }
       if (Input.GetKeyDown(KeyCode.Alpha1)&& hpBtnDelayTime <=0)
        {
            hpBtnDelayTime = 5f;
            OnClickHealHP();
        }
        if (mpBtnDelayTime > 0f)
        {
            mpBtnDelayTime -= Time.deltaTime;
        }
        if (Input.GetKeyDown(KeyCode.Alpha2) && mpBtnDelayTime <= 0)
        {
            mpBtnDelayTime = 5f;
            OnClickHealMP();
        }



        if (Input.GetKeyDown(KeyCode.L))
        {
            Debug.Log("lvlUp");
            currentLevel += 1;
            Invoke("LevelUp", 0.1f);
        }

        if (Input.GetKeyDown(KeyCode.M)) // Dùng skills sẽ bị trừ MP
        {
            TakeDamageMP(15);
        }

        // Máu và MP không xuống dưới 0.
        if (currentHP <= 0)
        {
            currentHP = 0;
        }
        if (currentMP <= 0)
        {
            currentMP = 0;
        }
        if (levelUp == true)
        {
            PowerBar.instance.hpSlider.maxValue = baseHP;
            PowerBar.instance.mpSlider.maxValue = baseMP;
            PowerBar.instance.hpSlider.value = baseHP;
            PowerBar.instance.mpSlider.value = baseMP;
            currentHP = baseHP;
            currentMP = baseMP;
            levelUp = false;
           
           
        }
      

    }

    public void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("EnemyWP"))
        {
            TakeDamage(150);
            Debug.Log("-150");

        }
    }
    //--------------------------------------------------------- Regen Update
   

    public void LevelUp()
    {
        currentLevel += 1;
        levelUp = true;
        baseHP += currentLevel * 20;
        baseMP += currentLevel * 10;

        baseATK += currentLevel * 2;
        baseDef += currentLevel * 2;

        currentHP = baseHP;
        currentMP = baseMP;

        lvlUpPanel.SetActive(true);
        Invoke("PanelLvlUp", 1f);
    }

    public void PanelLvlUp()
    {
        lvlUpPanel.SetActive(false);

    }
    public void TakeDamage(int damageHP) // logic  máu 
    {
        currentHP -= damageHP;
        hpBar.Health(currentHP);
        if (currentHP - damageHP <= (baseHP / 2))
        {
            if (hpRegenCheck != null)
            {
                StopCoroutine(hpRegenCheck);
            }
            hpRegenCheck = StartCoroutine(RegenHP());
        }

    }
   public void TakeDamageMP(int damageMP)  // logic Mana
    {
        currentMP -= damageMP;
        mpBar.Mana(currentMP);
        if (currentMP - damageMP <= (baseMP / 2))
        {
            if (mpRegenCheck != null)
            {
                StopCoroutine(mpRegenCheck);
            }
            mpRegenCheck = StartCoroutine(RegenMP());
        }
        
    }

    private IEnumerator RegenHP()
    {
        yield return new WaitForSeconds(5);

        while (currentHP < baseHP *0.6)// chỉ cho regen đến 2/3 thanh HP
        {
            currentHP += baseHP / 80 + currentLevel * 5;
            PowerBar.instance.hpSlider.value = currentHP;
            yield return hpRegen;
        }
        hpRegenCheck = null;
    }
    private IEnumerator RegenMP()
    {
        yield return new WaitForSeconds(5);

        while (currentMP < baseMP *0.6) // chỉ cho regen đến 2/3 thanh Mana
        {

            currentMP += baseMP / 70 + currentLevel * 5;
            PowerBar.instance.mpSlider.value = currentMP;
            yield return mpRegen;
        }
        mpRegenCheck = null;
    }


    public void RefillHP(int addHP)
    {
        currentHP += addHP;
      
        hpBar.Health(currentHP);
    }
           
    public void RefillMP(int addMP)
    {
        currentMP += addMP;
      
        mpBar.Mana(currentMP);
    }
   


   
    public GameObject hpPanel;
    public GameObject mpPanel;
    public void OnClickHealHP()
    {
        RefillHP(50);


        if ( currentHP <baseHP)
        {
            hpPanel.SetActive(true);
            Invoke("DelayHPHeal", 5f);
        }
       else
        {
            return;
        }
        
    }
    public void OnClickHealMP()
    {
        RefillMP(50);
        if (currentMP < baseMP)
        {
            mpPanel.SetActive(true);
            Invoke("DelayMPHeal", 5f);
        }
        
        
    }
    public void DelayHPHeal()
    {
        hpPanel.SetActive(false);
    }
    public void DelayMPHeal()
    {
        mpPanel.SetActive(false);
    }
}





