﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleport : MonoBehaviour
{
    public Transform teleportTarget;
    public Transform teleportTarget2;
    public GameObject player;

    
    public GameObject replayPanel;
       
    private void OnTriggerEnter(Collider other)
    {
        player.transform.position = teleportTarget.transform.position;
    }
    public void Update()
    {
        if (PlayerData.instance.currentHP <=0)
        {
            replayPanel.SetActive(true);
        }
    }

    public void OnClickReplay()
    {
        PlayerData.instance.currentHP = PlayerData.instance.baseHP;
        PlayerData.instance.currentMP = PlayerData.instance.baseMP;
        replayPanel.SetActive(false);
        player.transform.position = teleportTarget2.transform.position;
    }

}
