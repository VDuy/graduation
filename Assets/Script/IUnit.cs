﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IUnit 
{
    // Start is called before the first frame update
    Vector3 GetPosition();
    void SetPosition(Vector3 position);

}
