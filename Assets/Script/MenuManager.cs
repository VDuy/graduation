﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    [Header("MENU")]

    public GameObject uiMenu;
    public GameObject easyBtn;
    public GameObject normalBtn;
    public GameObject hardBtn;
    public GameObject uiMode;
    public GameObject uiSetting;
    public GameObject closeBtn;

    public int diff;


    public bool isEasy;
    public bool isNormal;
    public bool isHard;


    public static MenuManager instance;

    private void Awake()
    {
        instance = this;
    }


    public void OnClickPlayGame()
    {
        SceneManager.LoadScene(1);
        Time.timeScale = 1;
        UIManager.instance.isPause = false;
        //UIManager.instance.pause.SetActive(false);
        uiMode.SetActive(false);

    }
    public void OnClickReturnToMenu()
    {
        SceneManager.LoadScene(0);

    }
    public void OnClickResumeGame()
    {
        SceneManager.LoadScene(1);
        Time.timeScale = 1;
        UIManager.instance.isPause = false;
        //UIManager.instance.pause.SetActive(false);
        uiMode.SetActive(false);

        //load save data
    }
    public void OnClickChangeMode()
    {
        uiMenu.SetActive(false);
        uiMode.SetActive(true);
    }
    public void OnClickSetting()
    {
        uiMenu.SetActive(false);
        uiSetting.SetActive(true);
      
    }
    public void OnClickCloseButtonSetting()
    {

        closeBtn.SetActive(false);
        uiSetting.SetActive(false);
        //uiMode.SetActive(true);
        UIManager.instance.pause.SetActive(true);
    }
    public void OnClickNormalMode()
    {
        isNormal = true;
        isEasy = false;
        isHard = false;
        Time.timeScale = 1;
        UIManager.instance.pause.SetActive(true);
        uiMode.SetActive(false);
        Debug.Log("Normal");
        diff = 2;

    }
    public void OnClickEasyMode()
    {
        isEasy = true;
        isNormal = false;
        isHard = false;
        Time.timeScale = 1;
        UIManager.instance.pause.SetActive(true);
        uiMode.SetActive(false);

        Debug.Log("Easy");
        diff = 0;
    }

    public void OnClickHardMode()
    {
        isEasy = false;
        isNormal = false;
        isHard = true;
        UIManager.instance.pause.SetActive(true);
        uiMode.SetActive(false);

        Debug.Log("Hard");
        diff = 5;
    }
    public void OnClickQuitGame()
    {
        Application.Quit();
        Debug.Log("quit");
    }
}