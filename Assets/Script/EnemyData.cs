﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyData : MonoBehaviour
{
    public static EnemyData instance;
    public int wpDamage = 30;
    public int enemyHP = 200;

    public bool isDead;
    public GameObject hitSound;
    public bool isHitSoundOn = false;

  
    [SerializeField] public Animator animController;


    private void Awake()
    {
        instance = this;
    }

    public void Update()
    {
          
        if (enemyHP <= 0)
        {
            animController.SetBool("isKill", true);
            isDead = true;
          
        }

    }
    

    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Arrow") && !isDead)
        {
            isHitSoundOn = true;
            hitSound.SetActive(true);

            Invoke("EndHitSound", 0.5f);
            animController.Play("GetHit");
            enemyHP -= PlayerData.instance.totalAtk;
            Debug.Log("fire enemy");
        }
        
    }
    public void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player"))
        {

            animController.SetBool("attack", true);   
        }
    }
    public void OnTriggerExit(Collider other)
    {
       
        if (other.CompareTag("Player"))
        {
            animController.SetBool("attack", false);
        }
    }
   
    public void EndHitSound()
    {
        isHitSoundOn = false;
        hitSound.SetActive(false);
    }
}