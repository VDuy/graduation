﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UIManager : MonoBehaviour
{
    public static UIManager instance;
    //public GameObject uiManager;
    public GameObject closeInvBtn;
    public GameObject closeCharBtn;
    public GameObject inventory;
    public GameObject character;
    public GameObject pause;

    public GameObject weapon;

    public GameObject soundSystem;
    public bool isSoundOn;
    public GameObject attackSoundSystem;
    public bool isAttackSoundOn = false;

    public GameObject fBtn;
    public bool isActiveFBtn;
    public GameObject uiCloseShop;
    public GameObject uiShop;
    public bool isShopOpen;


    public GameObject closeShopBtn;
    public bool isCloseShop;

    public GameObject player;

    [Header("Bool")]
    public bool isPause;
    public bool isActiveInv;
    public bool isActiveChar;
    public bool isWeapon;
  

    // Start is called before the first frame update
    private void Awake()
    {
        instance = this;
    }
    void Start()
    {
        isShopOpen = false;
        isActiveFBtn = false;
        isPause = false;
        isActiveInv = false;
        isActiveChar = false;
        isCloseShop = false;
        isSoundOn = true;
        isWeapon = false;
    }

    // Update is called once per frame
    void Update()
    {
      

        if (Input.GetKeyDown(KeyCode.P))
        {
            if (!isPause)
            {
                Time.timeScale = 0;
                isPause = true;
                pause.SetActive(true);
            }
            else
            {
                Time.timeScale = 1;
                isPause = false;
                pause.SetActive(false);
            }
        }

        if (Input.GetKeyDown(KeyCode.I))
        {
            if (!isActiveInv)
            {
                OnClickInventoryButton();
            }
            else
            {
                OnClickCloseButton();
            }
        }
        if (Input.GetKeyDown(KeyCode.C))
        {
            if (!isActiveChar)
            {
                OnClickCharacterButton();
            }
            else
            {
                OnClickCloseCharacterButton();
            }
        }
        if (Input.GetKeyDown(KeyCode.Z))
        {
            if (!isWeapon)
            {
                WeaponOn();
            }
            else
            {
                WeaponOff();
            }
        }
    }

    public void WeaponOn()
    {
        weapon.SetActive(true);
        isWeapon = true;
    }
    public void WeaponOff()
    {
        weapon.SetActive(false);
        isWeapon = false;
    }
    public void OnClickInventoryButton()
    {
        //uiManager.SetActive(true);
        closeInvBtn.SetActive(true);
        inventory.SetActive(true);
        isActiveInv = true;
        //  Time.timeScale = 0;
    }

    public void OnClickCloseButton()
    {
        //uiManager.SetActive(false);
        closeInvBtn.SetActive(false);
        inventory.SetActive(false);
        isActiveInv = false;
        //    Time.timeScale = 1;
    }
    public void OnClickCharacterButton()
    {
        //uiManager.SetActive(true);
        closeCharBtn.SetActive(true);
        character.SetActive(true);
        isActiveChar = true;
        //  Time.timeScale = 0;
    }

    public void OnClickCloseCharacterButton()
    {
        //uiManager.SetActive(false);
        closeCharBtn.SetActive(false);
        character.SetActive(false);
        isActiveChar = false;
        //    Time.timeScale = 1;
    }

    public void PressF()
    {        
            uiShop.SetActive(true);
            uiCloseShop.SetActive(true);
            fBtn.SetActive(false);
            isShopOpen = true;
    }
    public void OnClickCloseShop()
    {
        
        uiShop.SetActive(false);
        uiCloseShop.SetActive(false);
        isShopOpen = false;
    }

    public void OnClickResume()
    {
        Time.timeScale = 1;
        isPause = false;
        pause.SetActive(false);
    }

    public void OnClickSoundSystemOn()
    {
        if (!isSoundOn)
        {
            soundSystem.SetActive(true);
            isSoundOn = true;
        }
        if (isSoundOn)
        {
            return;
        }
    }
    public void OnClickSoundSystemOff()
    {
        if (!isSoundOn)
        {
            return;
        }
        if (isSoundOn)
        {
            soundSystem.SetActive(false);
            isSoundOn = false;

        }
    }
}
